package com.example.starwarstest.api.models

interface ResourceResponse {
    fun getResourceType(): String
}