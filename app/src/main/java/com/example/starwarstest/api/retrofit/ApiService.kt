package com.example.starwarstest.api.retrofit

import FilmResponse
import PlanetResponse
import SpecieResponse
import StarshipResponse
import VehicleResponse
import com.example.starwarstest.api.models.PeopleResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

// ENDPOINT = https://swapi.dev/api/

interface ApiService {

    @GET("/api/people/")
    fun getPeopleList(@Query("page") pageNum: String): Observable<PeopleResponse>

    @GET("/api/starships/{id}")
    fun getStarship(@Path("id") id: String): Observable<StarshipResponse>

    @GET("/api/species/{id}")
    fun getSpecie(@Path("id") id: String): Observable<SpecieResponse>

    @GET("/api/films/{id}")
    fun getFilm(@Path("id") id: String): Observable<FilmResponse>

    @GET("/api/planets/{id}")
    fun getPlanet(@Path("id") id: String): Observable<PlanetResponse>

    @GET("/api/vehicles/{id}")
    fun getVehicle(@Path("id") id: String): Observable<VehicleResponse>

    @GET("/api/people/")
    fun searchPerson(@Query("search") name: String): Observable<PeopleResponse>

}
