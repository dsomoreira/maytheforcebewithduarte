package com.example.starwarstest.api

import android.content.Context
import com.example.starwarstest.api.models.PeopleResponse
import com.example.starwarstest.api.retrofit.ApiService
import com.example.starwarstest.api.retrofit.RetrofitBuilder
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent

class StarWarsAPI(context: Context): KoinComponent {

    private val api: ApiService by lazy { RetrofitBuilder(context).createApiService() }

    fun getPeopleList(pageNum: String): Observable<PeopleResponse> = api.getPeopleList(pageNum).subscribeOn(Schedulers.io()).observeOn(
        AndroidSchedulers.mainThread())

    fun getResource(type: String, id: String): Observable<*>? {
        return when(type){
            "starships" -> api.getStarship(id).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread())
            "species" -> api.getSpecie(id).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread())
            "films" -> api.getFilm(id).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread())
            "planet" -> api.getPlanet(id).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread())
            "vehicles" -> api.getVehicle(id).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread())
            else -> null
        }
    }

    fun searchPerson(name: String) : Observable<PeopleResponse> = api.searchPerson(name).subscribeOn(Schedulers.io()).observeOn(
        AndroidSchedulers.mainThread())
}
