package com.example.starwarstest

import com.example.starwarstest.mvi.home.PeopleListInteractor
import com.example.starwarstest.mvi.home.PeopleListPresenter
import com.example.starwarstest.repository.MainRepository
import com.example.starwarstest.api.StarWarsAPI
import com.example.starwarstest.api.retrofit.RequestInterceptor
import com.example.starwarstest.api.retrofit.RetrofitBuilder
import com.example.starwarstest.mvi.details.DetailsInteractor
import com.example.starwarstest.mvi.details.DetailsPresenter
import com.example.starwarstest.repository.IMainRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

class KoinModules {

    companion object {
        val mviPresenters = module {
            factory { MainRepository(get()) as IMainRepository }
            factory { PeopleListPresenter(PeopleListInteractor(get())) }
            factory { DetailsPresenter(DetailsInteractor(get())) }
        }

        val utils = module {
            single { StarWarsAPI(androidContext()) }
            single { RetrofitBuilder(androidContext())}
            single { RequestInterceptor(androidContext()) }
        }
    }
}