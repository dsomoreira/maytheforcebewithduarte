package com.example.starwarstest.mvi.util


interface DefaultViewState {

    object Loading : DefaultViewState

    class DataReceived(val data: Any) : DefaultViewState

    class Error(val error: Throwable) : DefaultViewState
}