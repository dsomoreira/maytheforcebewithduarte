package com.example.starwarstest.mvi.details

import com.example.starwarstest.menus.details.models.FetchDetailsModel
import com.example.starwarstest.mvi.util.DefaultViewState
import com.example.starwarstest.mvi.util.MviView
import io.reactivex.Observable

interface DetailsMviView : MviView<DefaultViewState> {
    fun loadIntent(): Observable<List<FetchDetailsModel>>
}