package com.example.starwarstest.mvi.details

import com.example.starwarstest.menus.details.models.DetailLink
import com.example.starwarstest.menus.details.models.FetchDetailsModel
import com.example.starwarstest.mvi.util.DefaultViewState
import com.example.starwarstest.repository.IMainRepository
import io.reactivex.Observable

class DetailsInteractor(private val repository: IMainRepository) {


    fun getDetails(list: List<FetchDetailsModel>): Observable<DefaultViewState> {
        val linkList = mutableListOf<DetailLink>()
        list.forEach { model ->
            model.links.forEach {
                val id = "\\d+".toRegex().find(it)?.value
                id?.let { linkList.add(DetailLink(model.type, id)) }
            }
        }
        return repository.getDetails(linkList).map {
            if(it.isNotEmpty())
                DefaultViewState.DataReceived(it)
            else
                DefaultViewState.Error(Throwable())
        }
    }
}