package com.example.starwarstest.mvi.home

import com.example.starwarstest.api.models.PersonData
import com.example.starwarstest.mvi.util.PeopleListViewState
import com.example.starwarstest.repository.IMainRepository
import io.reactivex.Observable

class PeopleListInteractor(private val repository: IMainRepository) {

    fun getPeopleList(): Observable<PeopleListViewState> {
        return repository.getPeopleList("1").flatMap {
            Observable.just((PeopleListViewState.DataReceived(it)) as PeopleListViewState)
        }
            .startWith(PeopleListViewState.Loading as PeopleListViewState)
            .onErrorReturn { PeopleListViewState.Error(it)}

    }

    fun fetchNextPage(pageNum: String): Observable<PeopleListViewState> {
        return repository.getPeopleList(pageNum).flatMap {
            Observable.just((PeopleListViewState.UpdateData(it)) as PeopleListViewState)
        }
            .startWith(PeopleListViewState.Loading as PeopleListViewState)
            .onErrorReturn { PeopleListViewState.Error(it)}

    }

    fun favoritePerson(data: PersonData, position: Int): Observable<PeopleListViewState> {
        return repository.sendWebhookPost(data).flatMap {
            Observable.just(PeopleListViewState.FavoritedResult(position, it.code) as PeopleListViewState)
        }
            .startWith(PeopleListViewState.Loading as PeopleListViewState)
            .onErrorReturn { PeopleListViewState.Error(it) }
    }

    fun searchPerson(name: String): Observable<PeopleListViewState> {
        return repository.searchPerson(name).flatMap{
            Observable.just(PeopleListViewState.SearchDataReceived(it) as PeopleListViewState)
        }
            .startWith(PeopleListViewState.Loading as PeopleListViewState)
            .onErrorReturn { PeopleListViewState.Error(it) }
    }
}