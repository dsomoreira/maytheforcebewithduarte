package com.example.starwarstest.mvi.home

import com.example.starwarstest.mvi.util.PeopleListViewState
import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class PeopleListPresenter(private val interactor: PeopleListInteractor) :
    MviBasePresenter<PeopleListMviView, PeopleListViewState>() {
    override fun bindIntents() {

        val loadIntent = intent(PeopleListMviView::loadIntent).flatMap { interactor.getPeopleList() }

        val fetchPageIntent = intent(PeopleListMviView::fetchPageIntent).flatMap { interactor.fetchNextPage(it) }

        val favoritePersonIntent = intent(PeopleListMviView::favoritePersonIntent).flatMap { interactor.favoritePerson(it.first, it.second) }

        val searchPersonIntent = intent(PeopleListMviView::searchPersonIntent).flatMap { interactor.searchPerson(it) }

        val viewState =
            Observable.merge(loadIntent, fetchPageIntent, favoritePersonIntent, searchPersonIntent)
                .observeOn(AndroidSchedulers.mainThread()).distinctUntilChanged()

        subscribeViewState(viewState, PeopleListMviView::render)
    }
}