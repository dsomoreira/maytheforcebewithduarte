package com.example.starwarstest.mvi.home

import com.example.starwarstest.api.models.PersonData
import com.example.starwarstest.mvi.util.MviView
import com.example.starwarstest.mvi.util.PeopleListViewState
import io.reactivex.Observable

interface PeopleListMviView :
    MviView<PeopleListViewState> {
    fun loadIntent(): Observable<Unit>
    fun fetchPageIntent(): Observable<String>
    fun favoritePersonIntent(): Observable<Pair<PersonData, Int>>
    fun searchPersonIntent(): Observable<String>
}