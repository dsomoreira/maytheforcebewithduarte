package com.example.starwarstest.mvi.util

import com.hannesdorfmann.mosby3.mvp.MvpView

interface MviView<T> : MvpView {

    fun render(viewState: T)

}