package com.example.starwarstest.mvi.details

import com.example.starwarstest.mvi.util.DefaultViewState
import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers

class DetailsPresenter(private val interactor: DetailsInteractor) :
    MviBasePresenter<DetailsMviView, DefaultViewState>() {
    override fun bindIntents() {

        val loadIntent = intent(DetailsMviView::loadIntent).flatMap { interactor.getDetails(it) }

        val viewState = loadIntent.observeOn(AndroidSchedulers.mainThread()).distinctUntilChanged()

        subscribeViewState(viewState, DetailsMviView::render)
    }
}