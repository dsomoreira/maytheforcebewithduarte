package com.example.starwarstest.mvi.util

import com.example.starwarstest.api.models.PeopleResponse


interface PeopleListViewState {

    object Loading : PeopleListViewState

    class UpdateData(val data: Any) : PeopleListViewState

    class DataReceived(val data: Any) : PeopleListViewState

    class Error(val error: Throwable) : PeopleListViewState

    class FavoritedResult(val position: Int, val responseCode: Int) : PeopleListViewState

    class SearchDataReceived(val data: PeopleResponse): PeopleListViewState
}