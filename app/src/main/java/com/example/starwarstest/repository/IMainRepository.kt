package com.example.starwarstest.repository

import com.example.starwarstest.api.models.PeopleResponse
import com.example.starwarstest.api.models.PersonData
import com.example.starwarstest.menus.details.models.DetailLink
import com.example.starwarstest.menus.details.models.DetailModel
import io.reactivex.Observable
import okhttp3.Response

interface IMainRepository {
    fun getPeopleList(pageNum: String): Observable<PeopleResponse>
    fun getDetails(list: List<DetailLink>): Observable<List<DetailModel>>
    fun sendWebhookPost(data: PersonData): Observable<Response>
    fun searchPerson(name: String): Observable<PeopleResponse>
}