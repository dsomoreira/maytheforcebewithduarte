package com.example.starwarstest.repository

import android.util.Log
import com.example.starwarstest.api.StarWarsAPI
import com.example.starwarstest.api.models.PeopleResponse
import com.example.starwarstest.api.models.PersonData
import com.example.starwarstest.menus.details.models.DetailLink
import com.example.starwarstest.menus.details.models.DetailModel
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.lang.Exception

class MainRepository(private val api: StarWarsAPI) : IMainRepository {

    override fun getPeopleList(pageNum: String): Observable<PeopleResponse> = api.getPeopleList(pageNum)

    override fun getDetails(list: List<DetailLink>): Observable<List<DetailModel>> = getAllLinkDetails(list)

    private fun getAllLinkDetails(list: List<DetailLink>): Observable<List<DetailModel>>{
        return Observable.fromIterable(list).flatMap{ detailLink ->
            api.getResource(detailLink.name, detailLink.value)?.map {
                listOf(DetailModel(detailLink.name, it))
            }
        }.toList().map { it.flatten() }.toObservable().onErrorReturn { emptyList() }
    }

    override fun sendWebhookPost(data: PersonData): Observable<Response> {
        val url = "https://webhook.site/8d7b8a94-a11e-4475-930d-89096b744159"
        val mediaType = "application/json; charset=utf-8".toMediaType()

        val json: String = Gson().toJson(data)

        val client = OkHttpClient()
        val body = json.toRequestBody(mediaType)
        val request = Request.Builder().url(url).post(body).build()

        return Observable.create<Response> {
            try {
                val response = client.newCall(request).execute()
                it.onNext(response)
                it.onComplete()
            } catch (e: Exception) {
                Log.d("Webhook POST ERROR", e.toString())
                it.onError(e.fillInStackTrace())
            }
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    }

    override fun searchPerson(name: String): Observable<PeopleResponse> = api.searchPerson(name)
}
