package com.example.starwarstest.menus.details.models

data class FetchDetailsModel(
    val type: String,
    val links: List<String>
)