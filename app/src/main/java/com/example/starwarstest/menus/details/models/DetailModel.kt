package com.example.starwarstest.menus.details.models

data class DetailModel(
    val type: String,
    val response: Any
)