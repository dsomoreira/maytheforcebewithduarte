package com.example.starwarstest.menus.home.models

import com.example.starwarstest.api.models.PersonData

data class  PeopleListUIModel(
    val data: PersonData,
    var favored: Boolean
)