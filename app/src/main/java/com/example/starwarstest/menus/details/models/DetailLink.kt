package com.example.starwarstest.menus.details.models

data class DetailLink(
    val name: String,
    val value: String
)