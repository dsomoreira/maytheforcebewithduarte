package com.example.starwarstest.menus.details

import FilmResponse
import PlanetResponse
import SpecieResponse
import StarshipResponse
import VehicleResponse
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.starwarstest.R
import com.example.starwarstest.api.models.PersonData
import com.example.starwarstest.menus.details.models.DetailModel
import com.example.starwarstest.menus.details.models.DetailsUIModel
import com.example.starwarstest.menus.details.models.FetchDetailsModel
import com.example.starwarstest.mvi.details.DetailsMviView
import com.example.starwarstest.mvi.details.DetailsPresenter
import com.example.starwarstest.mvi.util.DefaultViewState
import com.google.android.material.snackbar.Snackbar
import com.hannesdorfmann.mosby3.mvi.MviFragment
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.android.synthetic.main.fragment_details.loadingSpinner
import kotlinx.android.synthetic.main.fragment_details.view.*
import kotlinx.android.synthetic.main.fragment_people_list.*
import org.koin.android.ext.android.getKoin
import java.util.*


class DetailsFragment : MviFragment<DetailsMviView, DetailsPresenter>(), DetailsMviView {

    private val details by lazy { arguments?.get("results") as PersonData }
    private val loadIntent = BehaviorSubject.create<List<FetchDetailsModel>>()
    private val adapter = DetailAdapter()

    private lateinit var onBackListener: () -> Unit

    override fun loadIntent(): Observable<List<FetchDetailsModel>> = loadIntent

    override fun createPresenter(): DetailsPresenter = getKoin().get()

    @ExperimentalStdlibApi
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_details, container, false).apply {
            adapter.items = getPersonUIModel()
            detailsRv.adapter = adapter

            val list = getDetailsToFetch(details)

            loadIntent.onNext(list)
        }
    }

    override fun render(viewState: DefaultViewState) {
        when(viewState){

            is DefaultViewState.Loading -> {
                toggleLoading(true)
            }

            is DefaultViewState.DataReceived -> {
                val data = viewState.data as MutableList<DetailModel>
                data.map {
                    when(it.type){
                        "planet"    ->  listOf(DetailsUIModel("Homeworld", (it.response as PlanetResponse).name))
                        "species"   ->  listOf(DetailsUIModel("Species", (it.response as SpecieResponse).name))
                        "starships" ->  listOf(DetailsUIModel("Starships", (it.response as StarshipResponse).name))
                        "vehicles"  ->  listOf(DetailsUIModel("Vehicles", (it.response as VehicleResponse).name))
                        "films"     ->  listOf(DetailsUIModel("Films", (it.response as FilmResponse).title))
                        else        ->  listOf()
                    }
                }.map { adapter.addDetails(it) }

                toggleLoading(false)
            }

            is DefaultViewState.Error -> {
                Snackbar.make(detailsContent, "Error fetching details", Snackbar.LENGTH_LONG).show()
                toggleLoading(false)
            }
        }
    }

    private fun getDetailsToFetch(details: PersonData): List<FetchDetailsModel> =
        listOf(
            FetchDetailsModel("planet", listOf(details.homeworld)),
            FetchDetailsModel("species", details.species),
            FetchDetailsModel("films", details.films),
            FetchDetailsModel("starships", details.starships),
            FetchDetailsModel("vehicles", details.vehicles)
        )


    fun setOnBackListener(listener: () -> Unit) {
        onBackListener = listener
    }

    companion object{
        fun newInstance(person: PersonData): DetailsFragment {
            val fragment = DetailsFragment()
            val args = Bundle()
            args.putSerializable("results", person)
            fragment.arguments = args
            return fragment
        }
    }

    @ExperimentalStdlibApi
    private fun getPersonUIModel(): MutableList<DetailsUIModel> {
        details.apply {
            return mutableListOf(
                DetailsUIModel("Name", name),
                DetailsUIModel("Birthyear", birth_year),
                DetailsUIModel("Eye color", eye_color.capitalize(Locale.getDefault())),
                DetailsUIModel("Gender", gender.capitalize(Locale.getDefault())),
                DetailsUIModel("Height", height),
                DetailsUIModel("Weight", mass),
                DetailsUIModel("Created", created),
                DetailsUIModel("Hair color", hair_color.capitalize(Locale.getDefault())),
                DetailsUIModel("Skin color", skin_color.capitalize(Locale.getDefault()))
            )
        }

    }

    private fun toggleLoading(boolean: Boolean) {
        if (boolean)
            loadingSpinner.visibility = View.VISIBLE
        else
            loadingSpinner.visibility = View.GONE
    }

}