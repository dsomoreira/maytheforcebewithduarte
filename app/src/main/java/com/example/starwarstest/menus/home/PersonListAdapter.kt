package com.example.starwarstest.menus.home

import com.example.starwarstest.api.models.PersonData
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.starwarstest.R
import com.example.starwarstest.menus.home.models.PeopleListUIModel
import kotlinx.android.synthetic.main.person_item.view.*


class PersonListAdapter(private val loadMore: Boolean = true) : RecyclerView.Adapter<PersonListAdapter.PersonViewHolder>() {

    var items: MutableList<PeopleListUIModel> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private lateinit var onPersonClicked: (PersonData) -> Unit
    private lateinit var onFavoriteClicked: (PersonData, Int) -> Unit
    private lateinit var onBottomReachedListener: (Unit) -> Unit

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PersonViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.person_item, parent, false))

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {

        val item = items[position]
        val view = holder.itemView

        view.personName.text = item.data.name

        view.apply {
            if(item.favored)
                favoriteIcon.setBackgroundResource(R.drawable.ic_star_on)
            else
                favoriteIcon.setBackgroundResource(R.drawable.ic_star_off)

            personName.setOnClickListener {
                onPersonClicked.invoke(item.data)
            }

            favoriteIcon.setOnClickListener {
                onFavoriteClicked.invoke(item.data, position)
            }
        }

        if(loadMore && position == items.size - 1)
            onBottomReachedListener.invoke(Unit)

    }

    fun setOnPersonClicked(listener: (PersonData) -> Unit) {
        onPersonClicked = listener
    }

    fun setOnFavoriteClicked(listener: (PersonData, Int) -> Unit) {
        onFavoriteClicked = listener
    }

    fun setOnBottomReachedListener(listener: (Unit) -> Unit) {
        onBottomReachedListener = listener
    }

    fun addData(list: List<PeopleListUIModel>) {
        val oldSize = items.size
        items.addAll(list)
        notifyItemRangeInserted(oldSize, oldSize + list.size)
    }

    fun setFavored(pos: Int) {
        items[pos].favored = true
        notifyItemChanged(pos)
    }

    inner class PersonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}
