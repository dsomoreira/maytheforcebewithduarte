package com.example.starwarstest.menus.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.starwarstest.R
import com.example.starwarstest.api.models.PeopleResponse
import com.example.starwarstest.api.models.PersonData
import com.example.starwarstest.menus.home.models.PeopleListUIModel
import com.example.starwarstest.mvi.home.PeopleListMviView
import com.example.starwarstest.mvi.home.PeopleListPresenter
import com.example.starwarstest.mvi.util.PeopleListViewState
import com.google.android.material.snackbar.Snackbar
import com.hannesdorfmann.mosby3.mvi.MviFragment
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.fragment_people_list.*
import org.koin.android.ext.android.getKoin

class PeopleListFragment(bundle: Bundle?) : MviFragment<PeopleListMviView, PeopleListPresenter>(),
    PeopleListMviView {

    private var nextPage: String? = "1"
    private val loadIntent = BehaviorSubject.create<Unit>()
    private val fetchPageIntent = BehaviorSubject.create<String>()
    private val searchIntent = BehaviorSubject.create<String>()
    private val favoritePersonIntent = BehaviorSubject.create<Pair<PersonData, Int>>()
    private var adapter: PersonListAdapter? = null

    private val query: String? by lazy { bundle?.get(QUERY) as? String }

    private lateinit var onPersonClicked: (PersonData) -> Unit

    override fun createPresenter(): PeopleListPresenter = getKoin().get()

    override fun loadIntent()           = loadIntent
    override fun favoritePersonIntent() = favoritePersonIntent
    override fun searchPersonIntent()   = searchIntent
    override fun fetchPageIntent()      = fetchPageIntent


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_people_list, container, false).apply {
            adapter = query?.let { PersonListAdapter(false) } ?: PersonListAdapter()
            query?.let { searchIntent.onNext(query!!) } ?: loadIntent.onNext(Unit)
        }
    }

    override fun render(viewState: PeopleListViewState) {
        when (viewState) {

            is PeopleListViewState.Loading -> {
                toggleLoading(true)
            }

            is PeopleListViewState.DataReceived -> {
                val data = viewState.data as PeopleResponse
                nextPage = parseNextPageNumber(data.next)
                setAdapter(data)
                toggleLoading(false)
            }

            is PeopleListViewState.UpdateData -> {
                val data = viewState.data as PeopleResponse
                nextPage = parseNextPageNumber(data.next)
                updateAdapter(data)
                toggleLoading(false)
            }

            is PeopleListViewState.FavoritedResult -> {
                toggleLoading(false)

                if (viewState.responseCode == 200) {
                    adapter?.setFavored(viewState.position)
                    Snackbar.make(peopleListContent, "Webhook POST Sent", Snackbar.LENGTH_SHORT)
                        .show()
                } else
                    Snackbar.make(
                        peopleListContent, "Failed to POST Webhook", Snackbar.LENGTH_SHORT
                    ).show()
            }

            is PeopleListViewState.SearchDataReceived -> {
                val data = viewState.data
                setAdapter(data)
                toggleLoading(false)
            }

            is PeopleListViewState.Error -> {
                Snackbar.make(peopleListContent, "Error fetching content", Snackbar.LENGTH_LONG).show()
                toggleLoading(false)
            }
        }
    }

    fun setOnPersonClickedListener(listener: (PersonData) -> Unit) {
        onPersonClicked = listener
    }

    private fun parseNextPageNumber(next: String?): String? {
        return next?.let { "\\d+".toRegex().find(next)?.value }
    }

    private fun toggleLoading(boolean: Boolean) {
        if (boolean)
            loadingSpinner.visibility = View.VISIBLE
        else
            loadingSpinner.visibility = View.GONE
    }

    private fun setAdapter(data: PeopleResponse) {
        adapter?.apply {
            updateAdapter(data)

            val dividerItemDecoration =
                DividerItemDecoration(peopleListRv.context, LinearLayout.VERTICAL)
            peopleListRv.addItemDecoration(dividerItemDecoration)

            setOnPersonClicked {
                onPersonClicked.invoke(it)
            }

            setOnFavoriteClicked { personData: PersonData, pos: Int ->
                favoritePersonIntent.onNext(Pair(personData, pos))
            }

            setOnBottomReachedListener {
                nextPage?.let {
                    toggleLoading(true)
                    fetchPageIntent.onNext(nextPage!!)
                }
            }
        }

        peopleListRv.adapter = adapter
    }

    private fun updateAdapter(data: PeopleResponse) {
        val list = data.results.map { listOf(PeopleListUIModel(it, false)) }.toList().flatten()
        adapter?.addData(list)
    }

    companion object {
        private const val QUERY = "query"
        fun newInstance(query: String? = null) = PeopleListFragment(bundleOf(QUERY to query))
    }
}