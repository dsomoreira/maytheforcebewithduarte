package com.example.starwarstest.menus.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.starwarstest.R
import com.example.starwarstest.menus.details.models.DetailsUIModel
import kotlinx.android.synthetic.main.detail_item.view.*


class DetailAdapter : RecyclerView.Adapter<DetailAdapter.DetailsViewHolder>() {
    var items: MutableList<DetailsUIModel> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = items.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DetailsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.detail_item, parent, false)
        )


    override fun onBindViewHolder(holder: DetailsViewHolder, position: Int) {
        val item = items[position]
        val view = holder.itemView

        view.detailTitle.text = item.name
        view.detailValue.text = item.value

    }

    fun addDetails(details: List<DetailsUIModel>) {
        val oldSize = items.size

        details.forEach { itemToAdd ->
            val adapterItem = items.find { item -> item.name == itemToAdd.name }
            adapterItem?.apply {
                value = value + ", " + itemToAdd.value
            } ?: items.add(itemToAdd)
        }

        notifyItemRangeInserted(oldSize, oldSize + items.size)
    }

    inner class DetailsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}
