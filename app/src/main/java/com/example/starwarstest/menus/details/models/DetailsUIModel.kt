package com.example.starwarstest.menus.details.models

data class DetailsUIModel(
    val name: String,
    var value: String
)
