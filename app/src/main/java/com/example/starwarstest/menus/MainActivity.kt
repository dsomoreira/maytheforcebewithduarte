package com.example.starwarstest.menus

import android.os.Bundle
import android.view.Menu
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.starwarstest.KoinModules
import com.example.starwarstest.R
import com.example.starwarstest.menus.details.DetailsFragment.Companion.newInstance
import com.example.starwarstest.menus.home.PeopleListFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startKoin {
            modules(
                listOf(
                    KoinModules.mviPresenters,
                    KoinModules.utils
                )
            ).androidContext(applicationContext)
        }

        val peopleListFragment = PeopleListFragment.newInstance()
        setListeners(peopleListFragment)
        startFragment(peopleListFragment)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.fragments.size > 1) {
            supportFragmentManager.popBackStack()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menu.clear()
        menuInflater.inflate(R.menu.menu, menu).apply {
            val item = menu.findItem(R.id.action_search)
            val searchView = item.actionView as SearchView

            searchView.maxWidth = Int.MAX_VALUE

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    val searchFragment = PeopleListFragment.newInstance(query)
                    setListeners(searchFragment)
                    startFragment(searchFragment)

                    item.collapseActionView()
                    return false
                }

                override fun onQueryTextChange(p0: String?): Boolean {
                    return false
                }
            })
        }
        return true
    }

    private fun setListeners(fragment: PeopleListFragment) {
        fragment.setOnPersonClickedListener {
            val detailsFragment = newInstance(it)

            detailsFragment.setOnBackListener {
                supportFragmentManager.popBackStack()
            }
            startFragment(detailsFragment)
        }
    }

    private fun startFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .add(mainContent.id, fragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            .addToBackStack(null)
            .commit()
    }
}